Role Name
=========

Installs a goemaxima service

Requirements
------------

None yet.

Role Variables
--------------

- `goemaxima_docker_image`: Docker image to be used
- `goemaxima_internal_port`: port of container to be exposed (8080)
- `goemaxima_external_port`: port accessible from outside (80)

